import numpy as np
from numpy.typing import NDArray

def random_vectors(shape):
    vectors = np.random.rand(*shape)
    return vectors

def computational_basis(dim:int)->NDArray:
    return np.diag(np.ones(dim))

def scalar_product_along_axis(mat:NDArray[np.floating])->NDArray[np.floating]:
    return np.apply_along_axis(lambda x: np.dot(x, x), 0, mat)



