import numpy as np
from typing import Sequence
from numpy.typing import NDArray

from helper_functions import computational_basis

def normalize(arr:np.ndarray, axis=-1, order=2)->np.ndarray:
    """
    Taken from stack overflow
    https://stackoverflow.com/questions/21030391/how-to-normalize-a-numpy-array-to-a-unit-vector
    """
    l2 = np.atleast_1d(np.linalg.norm(arr, order, axis))
    l2[l2==0] = 1
    return arr / np.expand_dims(l2, axis)

def basis_from_points(points:NDArray)->np.ndarray:
    """
    Points is a N x N numpy ndarray. Points should be given like so np.array([p0, p1, p2, ..., pn-1])
    p0 will give the new origin.
    """
    assert_np_square_mat(points)
    def gram_schmidt_columns(X):
        Q, R = np.linalg.qr(X)
        return Q
    # we assume the first point will built the new origin.
    no_vecs = np.copy(points - points[0])
    no_vecs[0,:] = np.copy(points[0])
    o_vecs = gram_schmidt_columns(no_vecs)
    return o_vecs

def is_orthogonal_basis(basis:NDArray)->np.bool_:
    """
    we want to check if the matrix consisting of the basis vectors [b0, b1, ... ,bn-1] gives the kronecker delta
    upon matrix multiplication.
    """
    assert_np_square_mat(basis)
    res = np.matmul(basis.T, basis)
    expected_result = computational_basis(basis.shape[0])
    print(type(np.all(res==expected_result)))
    return np.all(res==expected_result)

def assert_np_square_mat(mat:NDArray)->None:
    assert type(mat) == np.ndarray, "Input should be of type np.ndarray"
    assert len(mat.shape) == 2, "Input should be a two D matrix"
    assert mat.shape[0] == mat.shape[1], "Input has to be a square matrix"

def random_points(scale:float, offset:float=0.0, dim:int=3)->np.ndarray:
    return scale * np.random.rand(dim) + offset

def point_in_new_basis(components:NDArray, old_basis:NDArray, new_basis:NDArray)->NDArray:
    """
    Given the components in `old_basis` return the components in the `new_basis`.
    """
    old_vec = components * old_basis
    new_components = np.array([np.dot(basis_vec, ov) for basis_vec, ov in zip(new_basis, old_vec)])
    return new_components

def shift_origin(vec:NDArray, shift_vec:NDArray)->NDArray:
    return vec + shift_vec

np.dot(np.array([1, 1, 1]), np.array([1, 1, 1]))

# np.dot(normalize(np.array([1, 1, 1])).T, normalize(np.array([1, 1, 1])))
